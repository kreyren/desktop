# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011 Mike Kazantsev <mk.fraggod@gmail.com>
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Marc-Antoine Perennou <keruspe@exherno.org>
# Distributed under the terms of the GNU General Public License v2

require spidermonkey python [ blacklist=3 has_lib=false has_bin=false multibuild=false ]
require flag-o-matic

DOWNLOADS="https://ftp.mozilla.org/pub/firefox/releases/${PV}esr/source/firefox-${PV}esr.source.tar.xz"

LICENCES="|| ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )"
SLOT="52"
PLATFORMS="~amd64 ~armv8 ~x86"
MYOPTIONS="jemalloc"

DEPENDENCIES="
    build:
        app-arch/zip
        sys-devel/autoconf:2.1
        virtual/pkg-config
    build+run:
        dev-libs/icu:=[>=58.2]
        dev-libs/libffi[>=3.1]
        dev-libs/nspr[>=4.13.1]
        sys-libs/readline:=
"

WORK=${WORKBASE}/firefox-${PV}esr/js/src

pkg_setup() {
    # spidermonkey build system requires that SHELL is always set.
    # It's missing sometimes in chroot environments, so force it here.
    export SHELL=/bin/sh
}

src_prepare() {
    default
    edo cd "${WORKBASE}/firefox-${PV}esr"
    # Those checks use hg/git
    for check in vanilla_allocations js_msg_encoding macroassembler_style spidermonkey_style; do
        echo "" > config/check_${check}.py
    done
    # Don't link dynamically to MOZ_GLUE in standalone mode (fix WIP upstream)
    edo sed -e 's/MOZ_GLUE_IN_PROGRAM=1/MOZ_GLUE_IN_PROGRAM=/' -e '/AC_DEFINE.MOZ_GLUE_IN_PROGRAM/d' -i old-configure.in -i js/src/old-configure.in
    edo autoconf-2.13 old-configure.in > old-configure
    edo touch configure js/src/configure
}

src_configure() {
    local myconf=(
        PYTHON=python2
        --host=$(exhost --build)
        --target=$(exhost --target)
        --prefix=/usr/$(exhost --target)
        --libdir=/usr/$(exhost --target)/lib
        --datadir=/usr/share
        --enable-intl-api
        --enable-readline
        --enable-ui-locale=en_US
        $(option_enable jemalloc)
        --with-system-nspr
        --with-nspr-prefix=/usr/$(exhost --target)
    )

    # set HOST_* for nsinstall
    HOST_CC="$(exhost --build)-cc"              \
    HOST_CFLAGS="$(print-build-flags CFLAGS)"   \
    HOST_LDFLAGS="$(print-build-flags LDFLAGS)" \
        edo ./configure "${myconf[@]}"
}

src_install() {
    default
    edo rm "${IMAGE}"usr/$(exhost --target)/lib/libjs_static.ajs
}

src_test() {
    esandbox allow_net "unix:${TEMP%/}/pymp-*/listener-*"
    default
    esandbox disallow_net "unix:${TEMP%/}/pymp-*/listener-*"
}

