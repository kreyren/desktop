# Copyright 2011 Johannes Nixdorf <mixi@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require launchpad [ suffix=tar.xz ] test-dbus-daemon python [ blacklist=3 multibuild=false ]
require vala [ vala_dep=true ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 ] ]

SUMMARY="Zeitgeist is a service which logs the users' activities and events"
HOMEPAGE+=" http://zeitgeist-project.com"

LICENCES="GPL-2 LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gobject-introspection"

DEPENDENCIES="
    build:
        dev-python/rdflib[python_abis:*(-)?]
        media-libs/raptor
        virtual/pkg-config[>=0.9.0]
    build+run:
        core/json-glib[>=0.14.0]
        dev-db/sqlite:3[>=3.7.11]
        dev-db/xapian-core
        dev-libs/glib:2[>=2.26.0]
        net-im/telepathy-glib[>=0.18.0][vapi]
        x11-libs/gtk+:3[>=3.0.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.30] )
        !dev-libs/zeitgeist:0 [[
            description = [ Slot move ]
            resolution = uninstall-blocked-after
        ]]
    run:
        dev-python/dbus-python[python_abis:*(-)?]
        gnome-bindings/pygobject:2[python_abis:*(-)?]
"

BUGS_TO="mixi@exherbo.org"

# The test are of questionable quality, they sometimes segfault, they try playing
# around with the live, running dbus, etc.
# Fix them if you dare.
RESTRICT="test"

# docs need valadoc
DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-datahub
    --enable-downloads-monitor
    --enable-fts
    --enable-telepathy
    --with-dee-icu=no
    --disable-docs
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gobject-introspection introspection'
)
# Tests fail to run randomly
DEFAULT_SRC_TEST_PARAMS=( -j1 )

src_prepare() {
    edo sed -e '/import rdflib/s/python/${PYTHON}/' -i configure.ac
    edo sed -e '/^#!/c #!'"${PYTHON}" -i data/ontology2code

    eautoreconf
    default
}

